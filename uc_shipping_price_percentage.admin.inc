<?php

/**
 * @file
 * Ubercart shipping per price percentage administration menu items.
 */

/**
 * percentage administrator.
 */
function uc_shipping_price_percentage_admin_settings($form, &$form_state) {
    // Container for credential forms
    $form['uc_shipping_price_percentage_fieldset'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('Percentage'),
      '#description'   => t('Percentage of shipping cost.'),
      '#collapsible'   => TRUE,
      '#collapsed'     => FALSE,
    );
    $form['uc_shipping_price_percentage_fieldset']['percentage'] = array(
        '#type' => 'textfield', 
        '#title' => t('Percentage'), 
        '#default_value' => variable_get('uc_shipping_price_percentage',0),
        '#field_suffix'=>'%',
        '#size' => 3, 
        '#maxlength' => 128, 
        '#required' => TRUE,
        '#element_validate' => array('element_validate_integer_positive'), 
    );
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save configuration'),
    );
    $form['actions']['cancel'] = array(
        '#markup' => l(t('Cancel'), 'admin/store/settings/quotes'),
    );
  
    if (!empty($_POST) && form_get_errors()) {
        drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
    }
    return $form;
}
function uc_shipping_price_percentage_admin_settings_validate($form, &$form_state) {
    if ($form_state['values']['percentage'] > 100) {
        form_set_error('percentage', t('the percentage is greater than 100.'));
    }
    if ($form_state['values']['percentage'] < 0) {
        form_set_error('percentage', t('the percentage does not have to be negative.'));
    }
}
function uc_shipping_price_percentage_admin_settings_submit($form, &$form_state) {
    variable_set('uc_shipping_price_percentage', $form_state['values']['percentage']);
    drupal_set_message(t('The configuration options have been saved.'));
    
    cache_clear_all();
    drupal_theme_rebuild();
}

